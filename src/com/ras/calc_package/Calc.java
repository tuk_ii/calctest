package com.ras.calc_package;

/**
 * Created by ras on 05.01.2018.
 */
public class Calc {
    private double buffer;              // настоящее число
    private double previousNumber;      // предыдущее число
    private String symbol;              // предыдущий символ
    // конструктор
    public Calc() {
        buffer = 0;
        previousNumber = 0;
        symbol = "";
    }

    // инициализация буфера
    public void setBuffer(double a) {

        buffer = a;
    }

    public double getBuffer() {

        return buffer;
    }

    public void clearBuffer() {

        buffer = 0;
    }
    // инициализация переменной предыдущего числа
    public void setPreviousNumber(double a) {

        previousNumber = a;
    }

    public double getPreviousNumber() {

        return previousNumber;
    }

    public void clearPreviousNumber() {

        previousNumber = 0;
    }
    // инициализация символа
    public String getSymbol() {

        return symbol;
    }

    public void setSymbol(String a) {

        symbol = a;
    }
    // функции
    public void doSumm(double a) {

        buffer += a;
    }

    public void doSub(double a) {

        buffer = a - buffer;
    }

    public void doMult(double a) {

        buffer *= a;
    }

    public void doDiv(double a) {

        buffer = a / buffer;
    }

    // разбор символов
    public void parseSymbol(String symbol) {
        switch (symbol) {
            case "=": {
                System.out.println(Double.toString(previousNumber));
                break;
            }
            case "+": {
                this.doSumm(previousNumber);
                break;
            }
            case "-": {
                this.doSub(previousNumber);
                break;
            }
            case "*": {
                this.doMult(previousNumber);
                break;
            }
            case "/": {
                this.doDiv(previousNumber);
                break;
            }
            default: {
                // если неизвестный символ
                throw new IllegalArgumentException();
            }
        }
    }

}
