package com.ras.calc_package;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

/**
 * Created by ras on 10.01.2018.
 */
public class CalcFunction {

    public static void calcRunner(String fileName) {

        try (BufferedReader br = new BufferedReader(new FileReader(fileName))) {

            String line;
            int num_counter = 0;            // счетчик для многозначных чисел
            final int BIT_CAPASITY = 10;    // множитель для многозначных чисел

            // считаем первое значение из файла чтобы синициализировать нужный калькулятор
            line = br.readLine();
            Calc calc = calcInitialization(line);
            calc.setSymbol("+");
            calc.clearPreviousNumber();

            while ((line = br.readLine()) != null) {

                if(isNumeric(line)) {
                    System.out.print(line);
                    calc.setBuffer(Double.parseDouble(line) + (calc.getBuffer() * BIT_CAPASITY * num_counter));
                    num_counter++;
                } else {
                    System.out.print(line);
                    calc.parseSymbol(calc.getSymbol());
                    calc.setSymbol(line);
                    calc.setPreviousNumber(calc.getBuffer());
                    calc.clearBuffer();
                    num_counter = 0;
                }
            }
            // если в файле больше нет строк - значит нужно произвести последнюю операцию
            calc.parseSymbol(calc.getSymbol());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static Calc calcInitialization(String type) {

        switch (type) {
            case "1":
            {
                return new Calc();
            }
            case "2":
            {
                return new CalcMem();
            }
            case "3":
            {
                return new EngineerCalc();
            }
            case "4":
            {
                return new EngineerCalcIMem();
            }
            default: {
                throw new IllegalArgumentException();
            }
        }
    }

    public static boolean isNumeric(String str)
    {
        try
        {
            double d = Double.parseDouble(str);
        }
        catch(NumberFormatException nfe)
        {
            return false;
        }
        return true;
    }

}
