package com.ras.calc_package;

/**
 * Created by ras on 05.01.2018.
 */
public class CalcMem extends Calc implements IMem {
    private double memValue;        // ячейка памяти

    public CalcMem() {
        memValue = 0;
    }

    // записать в ячейку памяти текущее значение
    public void memSave(double a) {

        memValue = a;
    }

    // очистить ячейку памяти
    public void memClear() {

        memValue = 0;
    }

    // присвоить текущему значению содержимое ячейки памяти
    public double memRead() {

        return memValue;
    }

    // увеличить(type = 1) или уменьшить(type = 2) значение в ячейке памяти на текущее значение
    public void memAdd(double a) {

        memValue += a;
    }

    public void memSub(double a) {

        memValue -= a;
    }

    @Override
    public void parseSymbol(String symbol) {
        switch (symbol) {
            case "MS": {
                this.memSave(this.getPreviousNumber());
                break;
            }
            case "MC": {
                this.memClear();
                break;
            }
            case "MR": {
                this.setBuffer(this.memRead());
                break;
            }
            case "M+": {
                this.memAdd(this.getPreviousNumber());
                break;
            }
            case "M-": {
                this.memSub(this.getPreviousNumber());
                break;
            }
            default: {
                super.parseSymbol(symbol);
            }
        }
    }
}
