package com.ras.calc_package;

/**
 * Created by ras on 05.01.2018.
 */
public class EngineerCalc extends Calc {

    public EngineerCalc() {
    }

    public void doCos(double a) {
        setBuffer(Math.cos(a));
    }

    public void doExp(double a) {
        setBuffer(Math.exp(a));
    }

    public void doSqrt(double a) {
        setBuffer(Math.sqrt(a));
    }

    @Override
    public void parseSymbol(String symbol) {
        switch (symbol) {
            case "cos": {
                this.doCos(this.getPreviousNumber());
                break;
            }
            case "exp": {
                this.doExp(this.getPreviousNumber());
                break;
            }
            case "sqrt": {
                this.doSqrt(this.getPreviousNumber());
                break;
            }
            default: {
                super.parseSymbol(symbol);
            }
        }
    }
}
