package com.ras.calc_package;

/**
 * Created by ras on 10.01.2018.
 */
public interface IMem {

    // записать в ячейку памяти текущее значение
    void memSave(double a);
    // очистить ячейку памяти
    void memClear();
    // присвоить текущему значению содержимое ячейки памяти
    double memRead();
    // увеличить или уменьшить значение в ячейке памяти на текущее значение
    void memSub(double a);
    void memAdd(double a);
}
